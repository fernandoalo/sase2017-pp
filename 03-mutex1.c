/*************************************************************************/
/* Ejemplos utilizados en el SASE 2017 - Programación Paralela           */
/* Fernando Aló - fernandoalo.utn@gmail.com - linkd.in/fernandoalo       */
/* Mariano Koremblum - nkoremblum@frba.utn.edu.ar - linkd.in/nmkoremblum */
/* Laboratorio de Procesamiento Digital - Departamento de Electrónica    */
/* Facultad Regional Buenos Aires / Universidad Tecnológica Nacional     */
/*************************************************************************/
/* Compilar con gcc -Wall -pthread in.c -o out                           */
/*************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define CANT_THREADS 20
int contador=0;

FILE *fd;
pthread_mutex_t mutex;

void *escribe (void *data){
	char *str = malloc (30);
	/* lockea el mutex para que nadie mas escriba en el archivo */
	pthread_mutex_lock(&mutex);
	
	printf("Mutex locked\n");
	sprintf(str,"%2i Soy el thread numero %ld\n",contador++,(unsigned long)pthread_self());

	printf("El contenido del buffer es: %s\n",str);
	printf("Escribo en el archivo\n");

	if(fprintf(fd,"%s",str) < 0 ){
		perror("fwrite");
		pthread_mutex_unlock(&mutex);
		free(str);
		pthread_exit(0);
	}

	free(str);
	/* se unlock el mutex para que otro thread pueda usar el archivo*/
	pthread_mutex_unlock(&mutex);
	printf("Mutex unlocked\n");
	pthread_exit(0);
}

int main(void){
	int i;
	pthread_t id[CANT_THREADS];

	printf("Abro archivo al que escribiran los threads\n");

	if ((fd = fopen("archivo.txt","w+")) == NULL ){
		perror("fopen()");
		return 1;
	}

	printf("Inicializo el mutex\n");
	pthread_mutex_init(&mutex,NULL);
	
	for(i=0;i<CANT_THREADS;i++){
	/*creacion del thread*/
		if(pthread_create(&id[i],NULL,&escribe,NULL)!= 0 ){
			perror("pthread_create");
			exit(1);
		}
	}
	//espero el join de todos los threads antes de salir.
	for(i=0;i<CANT_THREADS;i++)
		pthread_join(id[i],NULL);
	
	fclose(fd);
	printf("Cerré archivo\n");
	return 0;
}
