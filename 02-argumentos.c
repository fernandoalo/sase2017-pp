/*************************************************************************/
/* Ejemplos utilizados en el SASE 2017 - Programación Paralela           */
/* Fernando Aló - fernandoalo.utn@gmail.com - linkd.in/fernandoalo       */
/* Mariano Koremblum - nkoremblum@frba.utn.edu.ar - linkd.in/nmkoremblum */
/* Laboratorio de Procesamiento Digital - Departamento de Electrónica    */
/* Facultad Regional Buenos Aires / Universidad Tecnológica Nacional     */
/*************************************************************************/
/* Compilar con gcc -Wall -pthread in.c -o out                           */
/*************************************************************************/
//ps -t tty4 -o pid,ppid,stat,nlwp,pcpu,psr,cmd,comm H //para ver los threads


#define _GNU_SOURCE //necesario para pthread_setname_np()
#include <pthread.h>
#include <stdio.h>


struct char_print_parms{
        char character;
        int count;
};

void* char_print (void* parameters){
	struct char_print_parms* p = (struct char_print_parms*)parameters;
	int i;

	for(i = 0; i < p->count; ++i)
		fputc(p->character, stdout);
	return NULL;
}

int main(){
	pthread_t thread1_id;
	pthread_t thread2_id;
	struct char_print_parms thread1_args;
	struct char_print_parms thread2_args;

	
	thread1_args.character = 'X';
	thread1_args.count = 100000;
	pthread_create(&thread1_id, NULL, &char_print, &thread1_args);
	pthread_setname_np(thread1_id, "Thread-X"); //no es posix

	thread2_args.character = '-';
	thread2_args.count = 100000;
	pthread_create(&thread2_id, NULL, &char_print, &thread2_args);
	pthread_setname_np(thread2_id, "Thread--"); //no es posix

	pthread_join(thread1_id,NULL);
	pthread_join(thread2_id,NULL);

	return 0;
}

