/*************************************************************************/
/* Ejemplos utilizados en el SASE 2017 - Programación Paralela           */
/* Fernando Aló - fernandoalo.utn@gmail.com - linkd.in/fernandoalo       */
/* Mariano Koremblum - nkoremblum@frba.utn.edu.ar - linkd.in/nmkoremblum */
/* Laboratorio de Procesamiento Digital - Departamento de Electrónica    */
/* Facultad Regional Buenos Aires / Universidad Tecnológica Nacional     */
/*************************************************************************/
/* Compilar con gcc -Wall -pthread in.c -o out                           */
/*************************************************************************/

#include <pthread.h>
#include <stdio.h>

void* print_xs (void * unused){
  while(1)
    fputc('X',stderr);
  return NULL;
}

int main (void){
  pthread_t thread_id;	
  pthread_create(&thread_id,NULL,&print_xs,NULL);
  while(1)
    fputc('-',stderr);
  return 0;
}
